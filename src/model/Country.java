package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Country {
	private String code;
	private String name;
	
	private Connection conn;
	private Statement stmt;
	
	public Country() {
		try {
			//Connection
			java.sql.DriverManager.registerDriver(new com.mysql.jdbc.Driver());

			this.conn = DriverManager.getConnection("jdbc:mysql://192.168.64.2/Cities?user=root1&password=");

			//Create Statement
			this.stmt =  conn.createStatement();

		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList<Country> GetCountries(){
		try {

			//SQL
			String sql = "Select * from Countries";

			//Execute query
			ResultSet rs  = this.stmt.executeQuery(sql);

			ArrayList<Country> countries = new ArrayList<Country>();

			while (rs.next()) {
				Country item = new Country();
				item.setCode(rs.getString("code"));
				item.setName(rs.getString("name"));

				countries.add(item);
			}

			return countries;

		}catch (Exception ex) {
			System.out.println(ex.getMessage());
			return null;
		}
	}
	
}
	


