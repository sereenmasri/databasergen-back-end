package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Cities {
	private int idCity;
	private String name;
	
	private Connection conn;
	private Statement stmt;
	
	public int getIdCity() {
		return idCity;
	}
	public void setIdCity(int idCity) {
		this.idCity = idCity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList<Cafe> GetCafes(){
		try {

			//SQL
			String sql = "Select * from Cafes";

			//Execute query
			ResultSet rs  = this.stmt.executeQuery(sql);

			ArrayList<Cafe> cafes = new ArrayList<Cafe>();

			while (rs.next()) {
				Cafe item = new Cafe();
				item.setCafe(rs.getString("code"));
				item.setName(rs.getString("name"));

				cafes.add(item);
			}

			return GetCafes();

		}catch (Exception ex) {
			System.out.println(ex.getMessage());
			return null;
		}
	}}
