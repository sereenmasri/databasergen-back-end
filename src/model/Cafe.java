package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class 	Cafe {
	private static final String CafetoUpdate = null;
	private int idCafe;
	private String name;
	private String cafe;
	private boolean wifi;
	private String picture;
	private int idCity;
	
	
	//connection
	private Connection conn = null;
	private Statement stmt = null;
	
	public void setIdCafe(int idCafe) {
		this.idCafe = idCafe;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCafe() {
		return cafe;
	}
	public void setCafe(String cafe) {
		this.cafe = cafe;
	}
	public boolean isWifi() {
		return wifi;
	}
	public void setWifi(boolean wifi) {
		this.wifi = wifi;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public int getIdCity() {
		return idCity;
	}
	public void setIdCity(int idCity) {
		this.idCity = idCity;
	}

	//CRUD METHOD
	public int CreateCafe(String newCafe) {

		int newid=0;

		try {
			String sql = "INSERT INTO cafe (name) VALUES ('" + newCafe + "')";
			System.out.println(sql);

			int rowseffected = this.stmt.executeUpdate(sql);


			if (rowseffected  > 0) {
				sql = "SELECT MAX(idCafe) AS id FROM Cafe";

				//Execute query
				ResultSet rs  = this.stmt.executeQuery(sql);
				if (rs.next()) {
					newid = rs.getInt("id");
				}else {
					throw new Exception("Error to INSERT data");
				}
			}

			return newid;

		}catch(Exception ex){
			System.out.println(ex.getMessage());
			return 0;
		}
	}

	public ArrayList<Cafe> ReadCafe() {

		try {

			//SQL
			String sql = "Select * from Cafe";
			System.out.println(sql);

			//Execute query
			ResultSet rs  = this.stmt.executeQuery(sql);

			ArrayList<Cafe> cafes = new ArrayList<Cafe>();

			while (rs.next()) {
				Cafe item = new Cafe();
				item.setIdCafe(rs.getInt("idCafe"));
				item.setCafe(rs.getString("name"));

				cafes.add(item);
			}

			return cafes;

		}catch (Exception ex) {
			System.out.println(ex.getMessage());
			return null;
		}

	}

	public boolean UpdateCafe(int idCafe , String categorytoUpdate) {
		try {
			String sql = "UPDATE cafe SET name = '" + categorytoUpdate + "' WHERE idCafe ='" + idCafe + "'";
			System.out.println(sql);

			int rowseffected = this.stmt.executeUpdate(sql);

			if (rowseffected > 0)
				return true;
			else
				throw new Exception("Error to UPDATE data");

		}catch(Exception ex){
			System.out.println(ex.getMessage());
			return false;
		}
	}

	public boolean DeleteCafeByID(int idCafe) {
		try {
			String sql = "DELETE from Cafe  where idCafe  ='" + idCafe + "'";
			System.out.println(sql);

			int rowseffected = this.stmt.executeUpdate(sql);

			if (rowseffected > 0)
				return true;
			else
				throw new Exception("Error to DELETE data");

		}catch(Exception ex){
			System.out.println(ex.getMessage());
			return false;
		}
	}




}
