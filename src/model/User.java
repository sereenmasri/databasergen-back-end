package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class User {
	private static final ArrayList<User> User = null;
	private int IdUser;
	private String username;
	private String password;
	private String email;
	private String country;
	
	//connection
	private Connection conn = null;
	private Statement stmt = null;

	
	public int getIdUser() {
		return IdUser;
	}
	public void setIdUser(int idUser) {
		IdUser = idUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	
	//CRUD METHOD
	public int CreateUser(String newUser) {

		int newid=0;

		try {
			String sql = "INSERT INTO users (name) VALUES ('" + newUser + "')";
			System.out.println(sql);

			int rowseffected = this.stmt.executeUpdate(sql);

			if (rowseffected  > 0) {
				sql = "SELECT MAX(idUser) AS id FROM categories";

				//Execute query
				ResultSet rs  = this.stmt.executeQuery(sql);
				if (rs.next()) {
					newid = rs.getInt("id");
				}else {
					throw new Exception("Error to INSERT data");
				}
			}

			return newid;

		}catch(Exception ex){
			System.out.println(ex.getMessage());
			return 0;
		}
	}

	public ArrayList<User> ReadUsers() {

		try {

			//SQL
			String sql = "Select * from User";
			System.out.println(sql);

			//Execute query
			ResultSet rs  = this.stmt.executeQuery(sql);

			ArrayList<User> categories = new ArrayList<User>();

			while (rs.next()) {
				User item = new User();
				item.setIdUser(rs.getInt("IdUser"));
				item.setUsername(rs.getString("name"));

				User.add(item);
			}

			return User;

		}catch (Exception ex) {
			System.out.println(ex.getMessage());
			return null;
		}

	}


	
	
	public boolean UpdateUser(int idUser , String categorytoUpdate) {
		try {
			String sql = "UPDATE user SET name = '" + categorytoUpdate + "' WHERE idUser ='" + idUser + "'";
			System.out.println(sql);
			
			int rowseffected = this.stmt.executeUpdate(sql);

			if (rowseffected > 0)
				return true;
			else
				throw new Exception("Error to UPDATE data");

		}catch(Exception ex){
			System.out.println(ex.getMessage());
			return false;
		}
	}

	public boolean DeleteUserByID(int idUser) {
		try {
			String sql = "DELETE from User  where idUser  ='" + idUser + "'";
			System.out.println(sql);
			
			int rowseffected = this.stmt.executeUpdate(sql);

			if (rowseffected > 0)
				return true;
			else
				throw new Exception("Error to DELETE data");

		}catch(Exception ex){
			System.out.println(ex.getMessage());
			return false;
		}
	}



}

	

